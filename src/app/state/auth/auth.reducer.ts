import { Action, createFeatureSelector, createReducer, createSelector, on } from "@ngrx/store"
import { getUserSuccess, loginFailure, loginSuccess, logOut } from "./auth.actions";

export interface State{
  token: string | null,
  userName: string | null,
  errorMessage?: string;
}

export const initialState: State = {
  token: null,
  userName: null
}

const _authReducer = createReducer(
  initialState,
  on(loginSuccess, (state, {loginResponse}) => {
    return {
      ...state,
    token: loginResponse.responseMessage
    }
  }),
  on(loginFailure, (state, {error}) => {
    return {
      ...state,
      token: null,
      userName: null,
      errorMessage: error.responseMessage
    }
  }),
  on(logOut, () => {
    return initialState;
  }),
  on(getUserSuccess, (state, {user}) => {
    console.log(user)
    return {
      ...state,
      userName: user
    }
  })
);

export function authReducer(state: State | undefined, action: Action) {
  return _authReducer(state, action);
}

export const selectAuthState =  createFeatureSelector<State>('auth');

export const selectToken = createSelector(
  selectAuthState,
  (state: State) => state.token
);

export const selectUser = createSelector(
  selectAuthState,
  (state: State) => state.userName
);

