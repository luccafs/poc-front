import { ChartModel } from './../../interfaces/chartmodel.model';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SignalRService } from 'src/app/services/signal-r/signal-r.service';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import * as AuthActions from '../../state/auth/auth.actions';
import * as fromAuth from '../../state/auth/auth.reducer';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = 'home';

  public chartOptions: any = {
    scaleShowVerticalLines: true,
    responsive: true,
    scales: {
      y: {
        beginAtZero: true
      }
    }
  }

  public chartLabels: string[] = ["Real time data for the chart"];
  public chartType: string = "bar";
  public chartLegend: boolean = true;

  public userName = '';
  public token = '';

  constructor(
    public signalRService: SignalRService,
    private store: Store,
    private _store: Store<fromAuth.State>
  ){ }

  async ngOnInit(){
    //get token from state
    //auth token
    this.store.dispatch(AuthActions.getUser());
    //get auth user name from state
    this._store.select(fromAuth.selectUser).subscribe((user:any) => this.userName = user);

    this.callSignalR();
  }

  //Call SignalR service
  public callSignalR(){
    this.signalRService.startConnection();
    this.signalRService.addTransferChartDataListener();
    this.signalRService.addBroadcastChartDataListener();
    this.signalRService.startHttpRequest();
  }

  public chartClicked(){
    this.signalRService.broadcastChartData();
  }


  public signOut(){
      //localStorage.removeItem('token');
    this.store.dispatch(AuthActions.logOut());
  }
}
