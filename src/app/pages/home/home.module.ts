import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

import { NgChartsModule } from 'ng2-charts';
import { HttpClientModule} from '@angular/common/http'

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    NgChartsModule,
    HttpClientModule
  ],
  exports: [HomeComponent]
})
export class HomeModule { }
