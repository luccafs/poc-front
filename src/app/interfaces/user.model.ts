export interface NewUser{
  DsName: string,
  DsEmail: string,
  DsPassword: String
}

export interface Login{
  DsEmail: string,
  DsPassword: string
}
